import tensorflow as tf
from tensorflow.contrib.model_pruning.python import pruning # pylint: disable=no-name-in-module
from tensorflow.contrib.model_pruning.python.layers import layers # pylint: disable=no-name-in-module

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split




class Security:
    
    def __init__(self, sizes, eta, phi, alpha):
        
        tf.reset_default_graph()
        
        self.M = sizes[0]
        self.N = sizes[-1]
        self.H = sizes[1:-1]
        self.H_nb = len(self.H)
        
                
        
        #Input X: line flows (11 lines in the 6 bus system)
        self.X = tf.placeholder(tf.float32, [None, self.M])        
        
        with tf.variable_scope("Layers"):
            self.layer1 = layers.masked_fully_connected(self.X, self.H[0], scope='h1', 
                                                        weights_regularizer=tf.contrib.layers.l2_regularizer(scale=alpha))
            self.layer2 = layers.masked_fully_connected(self.layer1, self.H[1], scope='h2', 
                                                        weights_regularizer=tf.contrib.layers.l2_regularizer(scale=alpha))
            self.layer3 = layers.masked_fully_connected(self.layer2, self.H[2], scope='h3', 
                                                        weights_regularizer=tf.contrib.layers.l2_regularizer(scale=alpha))
            self.Ylogits = layers.masked_fully_connected(self.layer3, self.N, activation_fn=None, scope='h4', 
                                                        weights_regularizer=tf.contrib.layers.l2_regularizer(scale=alpha))
        self.Y = tf.nn.softmax(self.Ylogits)

        #Output labels: [0, 1] = safe, [1, 0] = unsafe
        self.Y_ = tf.placeholder(tf.float32, [None, self.N])
        
        #self.phi = tf.placeholder(tf.float32, [None])
        
        # Create global step variable (needed for pruning)
        self.global_step = tf.train.get_or_create_global_step()
        self.reset_global_step_op = tf.assign(self.global_step, 0)
        
        self.loss = -tf.reduce_mean(phi*self.Y_ * tf.nn.log_softmax(self.Ylogits)
                     + (1-self.Y_)*tf.nn.log_softmax(1-self.Ylogits)) 
                     #+  alpha*(tf.nn.l2_loss(self.W1) + tf.nn.l2_loss(self.W2))
                     #+  alpha*(tf.nn.l2_loss(self.W3) + tf.nn.l2_loss(self.W4)))
        
        self.train_step = tf.train.AdamOptimizer(eta).minimize(self.loss, global_step=self.global_step)
        
        # init
        init = tf.global_variables_initializer()
        self.sess = tf.Session()
        self.sess.run(init)
        
        
        
    def train_test_DB(self, DB, test_size=0.2, shuffle=True, save_splits=False):
        
        """ 
        DB contains all the [X1, X2, X3 ..., Xn, Y1, ... Ym], where Xn are the training points and Yn are the targets.
        ~ As of now Xn are the generator inputs, Y1=~Y2 stability class
        ===== ===== ===== =====
        Separation of the DB into a train and test sets 
        The DB must be a numpy.ndarray of shape (n_samples, n_lines + 1) with 
        DB[:, -1] = 0 if unsafe OP, 1 if safe OP
        DB[:, 0:nb_lines] = line flow
        DB[:, nb_lines:-1] = Generators outputs
        
        Returns the training and testing databases (warning: still including generators outputs)
        """
        nb_lines = self.M
        safe_state = DB[:, -1]

        classes = pd.get_dummies(safe_state, dtype=float)
        classes = np.array(classes)
        
        DB_train, DB_test, y_train, y_test = train_test_split(DB[:, 0:-1], 
                                                              classes,
                                                              test_size=test_size,
                                                              shuffle=shuffle)
        
        X_train = DB_train[:, 0:nb_lines]
        X_test = DB_test[:, 0:nb_lines]
        
        G_train = DB_train[:, nb_lines::]
        G_test = DB_test[:, nb_lines::]

        if(save_splits):
            np.savetxt('DB_train.csv', DB_train, delimiter=",")
            np.savetxt('DB_test.csv', DB_test, delimiter=",")
            np.savetxt('y_train.csv', y_train, delimiter=",")
            np.savetxt('y_test.csv', y_test, delimiter=",")

        return X_train, X_test, y_train, y_test, G_train, G_test
    
    
    
        
    def update_WB(self, Xtrain, Ytrain):
        
        train_data={self.X: Xtrain, self.Y_: Ytrain}
        self.sess.run(self.train_step, feed_dict=train_data)

        
            
        
    def train(self, Xtrain, Ytrain, X_test=None, y_test=None, batch_size=500, 
              nb_epochs=300, disp=None):
        
        nb_iteration = np.int(nb_epochs*np.ceil(len(Xtrain)/batch_size))
        nb_batch = (np.floor(len(Xtrain)/batch_size))*batch_size
        batch = 0
        epoch_nb = 1
        
        cross_val_train = pd.DataFrame(index=np.arange(nb_epochs)+1, 
                                       columns=['train accuracy', 'train recall', 'train spec'])
        cross_val_test = pd.DataFrame(index=np.arange(nb_epochs)+1,
                                      columns=['test accuracy', 'test recall', 'test spec'])

        for it in range(nb_iteration):
            
                
            Xbatch = Xtrain[batch:batch+batch_size, :]
            Ybatch = Ytrain[batch:batch+batch_size, :] 
        
            self.update_WB(Xbatch, Ybatch)  
            
                   
            
            if disp:
                print('Iteration: '+str(it)+' Batch: '+str(batch)+' Epoch nb: '+str(epoch_nb))


            if (batch == nb_batch) & (epoch_nb == nb_epochs):
                
                if X_test is not None:
                    _, _, ypred_test = self.predict(X_test, y_test)
                    _, r_test, _, s_test, a_test, _ = self.perf(ypred_test, y_test)
                    cross_val_test.loc[epoch_nb, :] = [a_test, r_test, s_test]
                _, _, ypred_train = self.predict(Xtrain, Ytrain)                                
                _, r_train, _, s_train, a_train, _ = self.perf(ypred_train, Ytrain)
                cross_val_train.loc[epoch_nb, :] = [a_train, r_train, s_train]
                
                break            
            
            
            if batch + batch_size > len(Xtrain):
                batch = - batch_size   
                epoch_nb += 1
                if X_test is not None:
                    _, _, ypred_test = self.predict(X_test, y_test)
                    _, r_test, _, s_test, a_test, _ = self.perf(ypred_test, y_test)
                    cross_val_test.loc[epoch_nb-1, :] = [a_test, r_test, s_test]
                _, _, ypred_train = self.predict(Xtrain, Ytrain)                                
                _, r_train, _, s_train, a_train, _ = self.perf(ypred_train, Ytrain)
                cross_val_train.loc[epoch_nb-1, :] = [a_train, r_train, s_train]
                
            
            batch += batch_size
            

                
        if X_test is not None:
            cross_val = pd.concat([cross_val_train, cross_val_test], axis=1)
        else:
            cross_val = cross_val_train        
        
        return cross_val

    def train_and_prune(self, Xtrain, Ytrain, X_test=None, y_test=None, batch_size=500, 
              nb_epochs=300, disp=None, pruning_step=0, end_pruning_step=250, pruning_frequency=1,
              sparsity_function_end_step=250, sparsity=.9, plot=False):
        
        # Get, Print, and Edit Pruning Hyperparameters
        pruning_hparams = pruning.get_pruning_hparams()
        print("Pruning Hyperparameters:", pruning_hparams)
        
        # Change hyperparameters to meet our needs
        pruning_hparams.begin_pruning_step = pruning_step
        pruning_hparams.end_pruning_step = end_pruning_step
        pruning_hparams.pruning_frequency = pruning_frequency
        pruning_hparams.sparsity_function_end_step = sparsity_function_end_step
        pruning_hparams.target_sparsity = sparsity      
        
        # Create a pruning object using the pruning specification, sparsity seems to have priority over the hparam
        p = pruning.Pruning(pruning_hparams, global_step=self.global_step, sparsity=sparsity)
        prune_op = p.conditional_mask_update_op()
        
        self.sess.run(tf.initialize_all_variables())
        
        self.sess.run(self.reset_global_step_op)
            
        nb_iteration = np.int(nb_epochs*np.ceil(len(Xtrain)/batch_size))
        nb_batch = (np.floor(len(Xtrain)/batch_size))*batch_size
        batch = 0
        epoch_nb = 1
        
        cross_val_train = pd.DataFrame(index=np.arange(nb_epochs)+1, 
                                       columns=['train accuracy', 'train recall', 'train spec'])
        cross_val_test = pd.DataFrame(index=np.arange(nb_epochs)+1,
                                      columns=['test accuracy', 'test recall', 'test spec'])

        for it in range(nb_iteration):
            Xbatch = Xtrain[batch:batch+batch_size, :]
            Ybatch = Ytrain[batch:batch+batch_size, :] 
        
            self.update_WB(Xbatch, Ybatch)
            
            # Prune and retrain
            self.sess.run(prune_op)
            self.sess.run(self.train_step, feed_dict={self.X: Xbatch, self.Y_: Ybatch})                      
            
            if disp:
                print('Iteration: '+str(it)+' Batch: '+str(batch)+' Epoch nb: '+str(epoch_nb))


            if (batch == nb_batch) & (epoch_nb == nb_epochs):
                
                if X_test is not None:
                    _, _, ypred_test = self.predict(X_test, y_test)
                    _, r_test, _, s_test, a_test, _ = self.perf(ypred_test, y_test)
                    cross_val_test.loc[epoch_nb, :] = [a_test, r_test, s_test]
                    
                _, _, ypred_train = self.predict(Xtrain, Ytrain)                                
                _, r_train, _, s_train, a_train, _ = self.perf(ypred_train, Ytrain)
                cross_val_train.loc[epoch_nb, :] = [a_train, r_train, s_train]
                
                break            
            
            
            if batch + batch_size > len(Xtrain):
                batch = - batch_size   
                epoch_nb += 1
                if X_test is not None:
                    _, _, ypred_test = self.predict(X_test, y_test)
                    _, r_test, _, s_test, a_test, _ = self.perf(ypred_test, y_test)
                    cross_val_test.loc[epoch_nb-1, :] = [a_test, r_test, s_test]
                    
                _, _, ypred_train = self.predict(Xtrain, Ytrain)                                
                _, r_train, _, s_train, a_train, _ = self.perf(ypred_train, Ytrain)
                cross_val_train.loc[epoch_nb-1, :] = [a_train, r_train, s_train]
                
                if (r_train > 99.5) & (s_train > 98):
                    break
                
            
            batch += batch_size
            

                
        if X_test is not None:
            cross_val = pd.concat([cross_val_train, cross_val_test], axis=1)
        else:
            cross_val = cross_val_train        
        
        if plot:
            p_cross = cross_val.dropna(axis='rows')
            p_cross.plot()
            plt.show()

        return cross_val


      
    def predict(self, Xtest, y_test):
          
          ypred, ylogits = self.sess.run([self.Y, self.Ylogits], feed_dict={self.X: Xtest, self.Y_: y_test}) 
                   
          ypred_max = np.argmax(ypred, 1)
          
          #if (ypred_max == 0).all():
          #    raise Warning("None of the safe OPs were correctly predicted !")
          
          return ypred, ylogits, ypred_max
      
    def perf(self, ypred, y_test):
        
        """ypred must be of shape: (Nsample,)"""
        
        pred_labels = ypred
        
        if y_test.shape[1] == 2:
            true_labels = y_test[:, 1]
        else: true_labels = y_test
        
        
        #if ypred.shape[1] == 2:
        #    pred_labels = ypred[:, 1]
        #else: pred_labels = ypred   
        
        TP = np.sum(np.logical_and(pred_labels == 0, true_labels == 0))
 
        # True Negative (TN): we predict a label of 0 (negative), and the true label is 0.
        TN = np.sum(np.logical_and(pred_labels == 1, true_labels == 1))
 
        # False Positive (FP): we predict a label of 1 (positive), but the true label is 0.
        FP = np.sum(np.logical_and(pred_labels == 0, true_labels == 1))
 
        # False Negative (FN): we predict a label of 0 (negative), but the true label is 1.
        FN = np.sum(np.logical_and(pred_labels == 1, true_labels == 0))
            
            
        recall = round(TP/(TP+FN)*100, 2)
        specificity = round(TN/(TN+FP)*100, 2)
        precision = round(TP/(TP+FP)*100, 2)
        accuracy = round((TP + TN)/(TP+FP+FN+TN)*100, 2)
        MCC = round((TP*TN - FP*FN)/np.sqrt((TP+FP)*(TP+FN)*(TN+FP)*(TN+FN))*100, 2)
        F1_score = 2*precision*recall/(precision+recall)
        
        return MCC, recall, precision, specificity, accuracy, F1_score
             
    def save_weights_bias(self, path=None):
        
        tvars = tf.trainable_variables()
        tvars_vals = self.sess.run(tvars)
        
        mask = self.sess.run(tf.contrib.model_pruning.get_masks())
        
        W = [tvars_vals[0], tvars_vals[2], tvars_vals[4], tvars_vals[6]]
        W1 = W[0]*mask[0]
        W2 = W[1]*mask[1]
        W3 = W[2]*mask[2]
        W4 = W[3]*mask[3]
        
        b1 = tvars_vals[1]
        b2 = tvars_vals[3]
        b3 = tvars_vals[5]
        b4 = tvars_vals[7]
        
        
        if path:
            np.savetxt(path+"/W0_p.csv", W1, delimiter=",")
            np.savetxt(path+"/b0_p.csv", b1, delimiter=",")
            np.savetxt(path+"/W1_p.csv", W2, delimiter=",")
            np.savetxt(path+"/b1_p.csv", b2, delimiter=",")
            np.savetxt(path+"/W2_p.csv", W3, delimiter=",")
            np.savetxt(path+"/b2_p.csv", b3, delimiter=",")          
            np.savetxt(path+"/W3_p.csv", W4, delimiter=",")
            np.savetxt(path+"/b3_p.csv", b4, delimiter=",")    
            
        return W1, b1, W2, b2, W3, b3, W4, b4
        

        
        

                  
          
          
          
          
          

              

        
                    
        
        
        
        
        
        
        
        
        
        